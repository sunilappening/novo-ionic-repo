import { Component } from '@angular/core';
import { NavController, ToastController,ModalController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { HomePage } from '../../pages/pages';
import { User } from '../../providers/user';
import { SignupPage } from '../signup/signup';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  responseData : any;
  
  credentials: { username: string, password: string } = {
    username: '',
    password: ''
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    public modalCtrl: ModalController) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  signup() {
    let modal = this.modalCtrl.create(SignupPage);
    modal.present();
  }

  // Attempt to login in through our User service
  doLogin() {
    console.log("inside doLogin")
    if (this.credentials.username && this.credentials.password) {
      this.user.login(this.credentials).then((result) => {
        this.responseData = result;
        if (this.responseData.meta.status='ok') {
          localStorage.setItem('userData', JSON.stringify(this.responseData.user))
          this.navCtrl.push(HomePage);
        }
        else {
          this.presentToast(this.loginErrorString);
        }
      }, (err) => {
        this.presentToast(this.loginErrorString);
      });
    }
    else {
      this.presentToast("Give username and password");
    }
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  
}

