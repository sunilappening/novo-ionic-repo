import { Component } from '@angular/core';
import { NavController, ModalController, ViewController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { Items } from '../../providers/providers';
import { User } from '../../providers/user';
import { Item } from '../../models/item';
import { News } from '../../models/news';
import { Alerts } from '../../models/alerts';
import {ClientPage} from '../client/client';

@Component({
  selector: 'page-main-page',
  templateUrl: 'main-page.html'
})
export class MainPage {
  currentItems: Item[];
  currentNews:  News[];
  recentActivities: Alerts[];
  
  responseData: any;

  constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController, 
              public viewCtrl: ViewController, public user: User, private iab: InAppBrowser) {
  }

  /**
   * The view loaded, let's query our news and recentActivities for the list
   */
  ionViewDidLoad() {
    console.log("inside ionViewDidLoad of main-page");
    let userData = localStorage.getItem('userData');
    this.items.getNews(userData).subscribe(res => {
      this.currentNews = res.news;
      this.recentActivities = res.alerts;
    },
      err => {
        // Log errors if any
        console.log(err);
      });

  }

  administer() {
    let modal = this.modalCtrl.create(ClientPage,{administer: 'administer'});
    modal.present();
  }

  scheduleAssessment(){
    let modal = this.modalCtrl.create(ClientPage,{administer: 'schedule'});
    modal.present();
  }

  emailAssessment(){
    let modal = this.modalCtrl.create(ClientPage,{administer: 'email'});
    modal.present();
  }
  

  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.items.delete(item);
  }


  splitTextName(detail){
    let stringToSplit = detail;
    let x = stringToSplit.split(" ");
    return x[0] + " " + x[1];
  }

  splitTextStatus(detail){
    let stringToSplit = detail;
    let x = stringToSplit.split(" ");
    return x[3] + " " + x[5];
  }

  itemSelected(url){
    const browser = this.iab.create(url,'_self',{location:'no'});
    //InAppBrowser.open(url, "_system", "location=true");
  }
  
}
