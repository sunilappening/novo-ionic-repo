import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Client } from '../models/client';
import { Api } from './api';

@Injectable()
export class Clients {
  clients: Client[];

  defaultItem: any = {
      "id": "84748",
      "user_id": "6324",
      "first_name": "Generic",
      "last_name": "Client",
      "sex_id": "3",
      "date_of_birth": "1800-01-01",
      "active": "1",
  };


  constructor(public http: Http, public api: Api) {
      console.log("inside constructor of client provider...")
      this.api.getClients('clients').subscribe(res => {
          this.clients = res;
      },
          err => {
              console.log(err);
          });


  }

  getClients(params?: any) {
      if (!params) {
          console.log("returning the clients list " + this.clients.length);
          return this.clients;
      }
      console.log("returning the filtered client list");
      //return this.clients.map(client => client.filter((item) => {
      return this.clients.filter((item) => {
              return (item.first_name.toLowerCase().indexOf(params.toLowerCase()) > -1);
       })
        
  }

  addClient(client: Client) {
      let seq =  this.api.post('clients', client).share();
        seq
          .map(res => res.json().data.client)
          .subscribe(res => {
              console.log(res);
            // If the API returned a successful response, push the response in the clientsList
            this.clients.push(res);
            console.log('added the client to the list below')
            console.log(this.clients);
            
          }, err => {
            console.error('ERROR', err);
          });

        return seq;
    
  }

  deleteClient(item: Client) {
    this.clients.splice(this.clients.indexOf(item), 1);
  }
}
