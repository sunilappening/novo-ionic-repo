import { Injectable } from '@angular/core';

import { Assessment } from '../models/assessment';
import { Api } from './api';

@Injectable()
export class Assessments {
  assessments: Assessment[];

  defaultItem: any = {
      "id": "84748",
      "user_id": "6324",
      "first_name": "Generic",
      "last_name": "Client",
      "sex_id": "3",
      "date_of_birth": "1800-01-01",
      "active": "1",
  };


  constructor(public api: Api) {
      console.log("inside constructor of assessments provider...")
      this.api.getAssessments('assessments', '0.5').subscribe(res => {
          this.assessments = res;
      },
          err => {
              console.log(err);
          });


  }

  getAssessments(params?: any) {
      if (!params) {
          return this.assessments;
      }
      return this.assessments.filter((item) => {
              return (item.short_name.toLowerCase().indexOf(params.toLowerCase()) > -1);
       })
        
  }

  

  
}
