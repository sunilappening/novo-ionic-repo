import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers,URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Notifications } from '../models/notifications';
import { Client } from '../models/client';
import { Assessment } from '../models/assessment';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
  //url: string = 'https://novopsych.com/api';
  url: string = 'http://localhost:8100/v1';
  
  credentials: any;

  constructor(public http: Http) {
    console.log("inside constructor of provide api");
    
  }

  get(endpoint: string, credentials) { 
    this.credentials = credentials;  
    return new Promise((resolve, reject) => {
      let headers = new Headers({ 'Content-Type': 'application/json' });
      headers.append('Access-Control-Allow-Origin', '*');
      let options = new RequestOptions({ headers: headers, withCredentials:credentials });
      this.http.get(this.url + '/' + endpoint, options)
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
        
    });
 }

  post(endpoint: string, body: any) {
    console.log(JSON.stringify(body));
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Credentials', 'true');
    headers.append('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
    headers.append('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
    let options = new RequestOptions({ headers: headers, withCredentials:this.credentials });
    var response =  this.http.post(this.url + '/' + endpoint, JSON.stringify(body),options);
      //.map(res => res.json().data)
      //.catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return response;
    
  }

  getClients(endpoint: string): Observable<Client[]> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Access-Control-Allow-Origin', '*');
    let options = new RequestOptions({ headers: headers, withCredentials: this.credentials });
    var response = this.http.get(this.url + '/' + endpoint, options)
      .map(res => <Client[]>res.json().data.clients)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return response;
  }

  getNews(endpoint: string, body): Observable<Notifications> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Access-Control-Allow-Origin', '*');
    let options = new RequestOptions({ headers: headers, withCredentials: this.credentials });
    var response = this.http.get(this.url + '/' + endpoint, options)
      .map(res => <Notifications>res.json().data)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return response;
  }

  getAssessments(endpoint: string, version: string): Observable<Assessment[]> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let assessmentVersion = new URLSearchParams();
    assessmentVersion.set('version', version);
    headers.append('Access-Control-Allow-Origin', '*');
    let options = new RequestOptions({ headers: headers, withCredentials: this.credentials, search: assessmentVersion });
    var response = this.http.get(this.url + '/' + endpoint + '/', options)
      .map(res => <Assessment[]>res.json().data.assessments)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    return response;
  }

  put(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + '/' + endpoint, body, options);
  }

  delete(endpoint: string, options?: RequestOptions) {
    return this.http.delete(this.url + '/' + endpoint, options);
  }

  patch(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + '/' + endpoint, body, options);
  }

  

}


