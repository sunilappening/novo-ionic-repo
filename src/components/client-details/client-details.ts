import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Client } from '../../models/client';

/**
 * Generated class for the ClientDetailsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'client-details',
  templateUrl: 'client-details.html'
})
export class ClientDetailsComponent {
  //@Input('clientId') clientIdDetails: Client
  @Input() client: Client;
  // text: string;

  // constructor() {
  //   console.log('Hello ClientDetailsComponent Component');
  //   this.text = "No Client Selected";
  // }

  // ngAfterViewInit(){
  //   this.text = "this.clientIdDetails.date_of_birth";
  // }

}
