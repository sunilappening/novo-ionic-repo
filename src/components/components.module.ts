import { NgModule } from '@angular/core';
import { ClientDetailsComponent } from './client-details/client-details';
import { AssessmentDetailsComponent } from './assessment-details/assessment-details';
@NgModule({
	declarations: [ClientDetailsComponent,
    AssessmentDetailsComponent],
	imports: [],
	exports: [ClientDetailsComponent,
    AssessmentDetailsComponent]
})
export class ComponentsModule {}
