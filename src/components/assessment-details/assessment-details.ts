import { Component,Input } from '@angular/core';
import { Assessment } from '../../models/assessment';

/**
 * Generated class for the AssessmentDetailsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'assessment-details',
  templateUrl: 'assessment-details.html'
})
export class AssessmentDetailsComponent {

  @Input() assessment: Assessment;


}
